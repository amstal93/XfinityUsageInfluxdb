# Xfinity Usage InfluxDB

Uses [xfinity-usage](https://github.com/jantman/xfinity-usage) to scrap Xfinity Bandwidth usage on a cron schedule and save the output to InfluxDB. Used in [HomelabOS](http://homelabos.com)

## Building

`docker build .`

## Using

Set your environment variables:

XFINITY_USER
XFINITY_PASSWORD
INFLUXDB_HOST

`docker run nickbusey/xfinityusageinfluxdb`

## Sample Compose

```
version: '2'

services:
  xfinityusageinfluxdb:
    restart: unless-stopped
    image: nickbusey/xfinityusageinfluxdb
    links:
      - influxdb
    environment:
      - XFINITY_USER=youruser
      - XFINITY_PASSWORD=yourpass
      - INFLUXDB_HOST=192.168.1.123
```
